package se.experis.springbootBySven.Controllers;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class HelloController {



    @GetMapping("/hello")//mapping for /hello is called
    public String greetGuest(){
        return "Hello, Guest!";
    }

    @RequestMapping(value = "/greeting",params = "name", method = RequestMethod.GET) //used when greeting and the text from the js-method is called
    public String displayGreetin(@RequestParam("name") String displaynName) {//making a string out of what comes after "name" in the url
        return "Welcome and hello " + displaynName;//returning a greetmessage and a name
    }

    @RequestMapping(value= "/reverseGreeting", params = "reversename", method = RequestMethod.GET)//same as above method
    public String displayGreeting(@RequestParam("reversename") String displayReverseName){
        String returnString;
        returnString = new StringBuilder(displayReverseName).reverse().toString();//reversing the string with the stringbuilder
        return "Here is your text reversed: " + returnString;
    }







}
