package se.experis.springbootBySven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBySvenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBySvenApplication.class, args);
	}

}
